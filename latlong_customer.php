<!DOCTYPE html>
<html lang="en" >
    <head>
        <meta charset="utf-8" />
        <meta name="author" content="Script Tutorials" />
        <title>Finding staffs nearby</title>  
		  <script src="http://maps.google.com/maps/api/js?sensor=false" 
          type="text/javascript"></script>
		  <style>
		  /* -------------------------------------- */

			ol{
				counter-reset: li;
				list-style: none;
				*list-style: decimal;
				font: 15px 'trebuchet MS', 'lucida sans';
				padding: 0;
				margin-bottom: 4em;
				text-shadow: 0 1px 0 rgba(255,255,255,.5);
			}

			ol ol{
				margin: 0 0 0 2em;
			}

			/* -------------------------------------- */			

			.rounded-list a{
				position: relative;
				display: block;
				padding: .4em .4em .4em 2em;
				*padding: .4em;
				margin: .5em 0;
				background: #ddd;
				color: #444;
				text-decoration: none;
				-moz-border-radius: .3em;
				-webkit-border-radius: .3em;
				border-radius: .3em;
				-webkit-transition: all .3s ease-out;
				-moz-transition: all .3s ease-out;
				-ms-transition: all .3s ease-out;
				-o-transition: all .3s ease-out;
				transition: all .3s ease-out;	
			}

			.rounded-list a:hover{
				background: #eee;
			}

			.rounded-list a:hover:before{
				-moz-transform: rotate(360deg);
			  	-webkit-transform: rotate(360deg);
			    -moz-transform: rotate(360deg);
			    -ms-transform: rotate(360deg);
			    -o-transform: rotate(360deg);
			    transform: rotate(360deg);	
			}

			.rounded-list a:before{
				content: counter(li);
				counter-increment: li;
				position: absolute;	
				left: -1.3em;
				top: 50%;
				margin-top: -1.3em;
				background: #87ceeb;
				height: 2em;
				width: 2em;
				line-height: 2em;
				border: .3em solid #fff;
				text-align: center;
				font-weight: bold;
				-moz-border-radius: 2em;
				-webkit-border-radius: 2em;
				border-radius: 2em;
				-webkit-transition: all .3s ease-out;
				-moz-transition: all .3s ease-out;
				-ms-transition: all .3s ease-out;
				-o-transition: all .3s ease-out;
				transition: all .3s ease-out;
			}
			</style>
    </head>
    <body>
<?php
	 if (isset($_GET["q"])) $Address = urlencode($_GET["q"]);
  //$Address = urlencode($Address);http://maps.google.com/maps?&q=54+Nguyen+Trai%2C+Quan+1%2C+ho+chi+minh%2C+JP01%2C+7000%2C+JP&z=16
  $request_url = "http://maps.googleapis.com/maps/api/geocode/xml?address=".$Address."&sensor=true";
  $xml = simplexml_load_file($request_url) or die("url not loading");
  $status = $xml->status;
  if ($status=="OK") {
      $Lat = $xml->result->geometry->location->lat;
      $Lon = $xml->result->geometry->location->lng;
      $LatLng = "$Lat,$Lon";
  }
  //echo $LatLng;
include_once('wp-config.php');
$username = DB_USER;
$password = DB_PASSWORD;
$hostname = DB_HOST; 

//connection to the database
$dbhandle = mysql_connect($hostname, $username, $password) 
  or die("Unable to connect to MySQL");
$selected = mysql_select_db(DB_NAME,$dbhandle) 
  or die("Could not select examples");
mysql_query("DELETE FROM wp_latlng") or die(mysql_error());

$loc_result = mysql_query("SELECT post_id,post_title,meta_value FROM wp_posts LEFT JOIN wp_postmeta ON wp_posts.ID=wp_postmeta.post_id WHERE wp_postmeta.meta_key='_kad_project_location'");
//mysql_query("CREATE TEMPORARY TABLE IF NOT EXISTS table1 (`id` INT(11) NOT NULL AUTO_INCREMENT,`name` text NOT NULL,`lat` double(10,7) NOT NULL,`lng` double(10,7) NOT NULL) ENGINE MyISAM");
while ($loc_row = mysql_fetch_array($loc_result)) {
		//echo $loc_row{'meta_value'};
	  $loc_request_url = "http://maps.googleapis.com/maps/api/geocode/xml?address=".$loc_row{'meta_value'}."&sensor=true";
	  $loc_xml = simplexml_load_file($loc_request_url) or die("url not loading");
	  $loc_status = $loc_xml->status;
	  $name = $loc_row{'post_title'};
	  $postID = $loc_row{'post_id'};
	  if ($loc_status=="OK") {
		  $lLat = $loc_xml->result->geometry->location->lat;
		  $lLon = $loc_xml->result->geometry->location->lng;
		  //$loc_LatLng = "$lLat,$lLon";
	  }
	  //echo $loc_LatLng;
	mysql_query("INSERT INTO wp_latlng (`postID`,`name`,`lat`,`lng` ) VALUES ('$postID','$name','$lLat','$lLon')") or die(mysql_error());
}

//execute the SQL query and return records
$result = mysql_query("SELECT id, postID, name, lat, lng, ( 3959 * acos( cos( radians($Lat) ) * cos( radians( lat ) ) * 
cos( radians( lng ) - radians($Lon) ) + sin( radians($Lat) ) * 
sin( radians( lat ) ) ) ) AS distance FROM wp_latlng HAVING
distance < 20 ORDER BY distance LIMIT 0 , 20");
//fetch tha data from the database

$location = array();
echo "<div style='float:left;width: 19%;'><div style='background-color:#e2e2e2;text-align:center;padding:5px 0;'>Staffs nearby</div><ol class='rounded-list'>";
while ($row = mysql_fetch_array($result)) {
	$km = round($row{'distance'}*1.609344,2);
   echo "<li><a href='wp-admin/post.php?post=".$row{'postID'}."&action=edit'>".$row{'name'}." - ".$km."km</a></li>";
   $location[] = array('name'=>$row{'name'},'lat'=>$row{'lat'},'lng'=>$row{'lng'});
}
echo "</ol></div>";
mysql_close($dbhandle);
?>
<div id="map" style="height: 650px; width:80%;float:right;"></div>

  <script type="text/javascript">
    var locations = [
		['Customer',<?php echo $Lat?>, <?php echo $Lon?>,'http://maps.google.com/mapfiles/ms/icons/red-dot.png',1],	 
		<?php $i=2; foreach($location as $key => $value){
			echo "['".$value['name']."',".$value['lat'].",".$value['lng'].",'http://maps.google.com/mapfiles/ms/icons/blue-dot.png',".$i."],";$i++;
		}?> 
		 
    ];
	
	var stylez = [
    {
      featureType: "all",
      elementType: "all",
      stylers: [
        { saturation: -100 } // <-- THIS map in grayscale
      ]
    }
];
	
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 14,
      center: new google.maps.LatLng(<?php echo $Lat?>, <?php echo $Lon?>),
      mapTypeId: google.maps.MapTypeId.ROADMAP,
    });
	
	var mapType = new google.maps.StyledMapType(stylez, { name:"Grayscale" });    
	map.mapTypes.set('map', mapType);
	map.setMapTypeId('map');

    var infowindow = new google.maps.InfoWindow();

    var marker, i;
	
    for (i = 0; i < locations.length; i++) {	
		  marker = new google.maps.Marker({
			position: new google.maps.LatLng(locations[i][1], locations[i][2]),
			map: map,
			//icon: 'http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld='+i+'|ff0000|000000' 
			icon: locations[i][3],
		  });		
      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  </script>
</body>
</html>
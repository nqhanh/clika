<?php
/*
Template Name: Nursing care
*/
?>


<div class="container">
<div class="margin-medium"></div>
<div><h1 class="medium-font uppercase text-stroke blue bold">訪問介護（ホームヘルプ）</h1>
</div>
</div>


<section class="slash-bg">
<div id="blog_carousel_container" class="container carousel_outerrim">

	<div class="blog-carouselcase fredcarousel">
		<div id="carouselcontainer">
			<div class="caroufredsel_wrapper" style="display: block; text-align: start; float: none; top: auto; right: auto; bottom: auto; left: auto; z-index: auto; width: 100%; margin: 0px;">
				<div id="blog_carousel" class="blog_carousel caroufedselclass clearfix">
					<div class="tcol-md-6 tcol-sm-6 tcol-xs-6 tcol-ss-12 service-intro">
						<div class="margin-medium"></div>
						<h5><span class="linebrk">ご家族の介護負担を少しでも軽減する事が私たちの目標です。</span></h5><div class="margin-small"></div>
						<p><span class="linebrk">ご本人様にとって無理のない自立支援、またご家族様の介護負担の軽減にお役に立てるサービスをご提供致します。</p><div style="clear:both;"></div>
						<p><span class="linebrk">詳しいサービス内容に関してはご本人様の要介護やご家族のサポート環境により異なります。</span><span class="linebrk">サービスお申し込み前に事前にお問い合わせください。</span></p>
						<div class="margin-small" style="clear:both;"></div><p class="bold">■インターネット申込みは24時間受付中</p>
						<!--<div class="margin-small"></div>
						<span class="star-burst-nursing"><span><span><span><p><a class="text-stroke-shadow white bold">料金<br>1回利用<br>1回2時間</a><p class="text-stroke-flat yellow bold" style="font-size: 25px;">4,000円</p></p></span></span></span></span>-->
						<div class="button-action"><a class="btn btn-lg btn-primary" href="#calendar">サービス仮申し込み <i class="fa fa-chevron-circle-down"></i></a></div>
					</div>
					
					<div class="tcol-md-3 tcol-sm-3 tcol-xs-6 tcol-ss-12 serice-content">
						<div class="blog_item grid_item">
							<div class="imghoverclass"> 
									<img src="<?php echo bloginfo('template_directory');?>/assets/img/maternity.jpg" alt="生活援助" class="iconhover" style="display:block;">
							</div> 
							<!--<div class="housekeeping-box">
								<header>
									<h5 class="entry-title">身体介護</h5>
									
								</header>
								<div class="entry-content">
									<p>身体介護とは、利用者の身体に直接触れて行う介助や、その準備・後始末、利用者の機能向上のための介助や専門的援助をいいます。
日常的な介護を必要とする方に、身体機能向上のための適切なサービスをご提供いたします。</p><div style="clear:both;"></div>
								</div> 
							</div> -->
						</div>
					</div>
					<div class="tcol-md-3 tcol-sm-3 tcol-xs-6 tcol-ss-12 serice-content">
						<div class="post-143 post type-post status-publish format-standard has-post-thumbnail hentry category-photos tag-carousel tag-gallery tag-photos blog_item grid_item">
							<div class="imghoverclass"> 
								
									<img src="<?php echo bloginfo('template_directory');?>/assets/img/cooking.jpg" alt="Image Gallery Post" class="iconhover" style="display:block;"> 
								
							</div> 
						<!--<div class="housekeeping-box">
							<header><h5 class="entry-title">生活支援</h5></header>
							<div class="entry-content">
								<p>ご利用者が単身、もしくはご家族が障害・疾病などで家事を行うことが困難な場合の訪問介護で、身体介護以外の掃除・洗濯・排泄など、日常生活援助を行うサービスです。</p><div style="clear:both;"></div>
							</div> 
						</div>-->
						<span class="star-burst"><span><span><span><p><a class="text-stroke-shadow white bold">料金<br>1回利用<br>1回2時間</a><p class="text-stroke-flat yellow bold" style="font-size: 25px;">4,000円</p></p></span></span></span></span>
						</div>
					</div>
					<!--<div class="tcol-md-3 tcol-sm-3 tcol-xs-6 tcol-ss-12 serice-content">
						<div class="post-106 post type-post status-publish format-standard has-post-thumbnail hentry category-adventure tag-adventure tag-fun tag-photos blog_item grid_item">
							<div class="imghoverclass"> 
								<img src="<?php echo bloginfo('template_directory');?>/assets/img/maternity.jpg" alt="Outdoor Adventure" class="iconhover" style="display:block;"> 
							</div> 
						<div class="housekeeping-box">
							<header>
								<h5 class="entry-title">身体介護<br><span class="text-primary">お客様のお身体の状態に合わせ、専門的な介護サービスをご提供します</span></h5>
									
							</header>
							<div class="entry-content">
								<p><span class="linebrk">介護スタッフがご自宅を訪問し、入浴の</span><span class="linebrk">介助、更衣介助、おむつの交換など身体</span><span class="linebrk">に関するお手伝いをします。</span></p><div style="clear:both;"></div>
							</div> 
						</div>
						
						</div>
					</div>-->
					
				</div>
			</div>
		
		</div>
	</div>
</div>
</section>

<div class="margin-medium"></div>

    <div id="content" class="container">
   		<div class="row">
     		<div class="main <?php //echo kadence_main_class(); ?>" role="main">
				<?php get_template_part('templates/content', 'page'); ?>
			</div><!-- /.main -->
<?php
/*
Template Name: Housekeeping
*/
?>


<div class="container">
<div class="margin-medium"></div>
<div><h1 class="medium-font uppercase text-stroke blue bold">ハウスキーピング(家事代行)サービス</h1>
</div>
</div>




<section class="slash-bg">
<div id="blog_carousel_container" class="container carousel_outerrim">

	<div class="blog-carouselcase fredcarousel">
		<div id="carouselcontainer">
			<div class="caroufredsel_wrapper" style="display: block; text-align: start; float: none; top: auto; right: auto; bottom: auto; left: auto; z-index: auto; width: 100%; margin: 0px;">
				<div id="blog_carousel" class="blog_carousel caroufedselclass clearfix">
					<div class="tcol-md-6 tcol-sm-6 tcol-xs-6 tcol-ss-12 service-intro">
					
						<div class="margin-medium"></div>
						<h5>心を込めたサービスをご提供いたします。</h5>
						<p><span class="linebrk">忙しくて家事の時間が取れない、週末はお掃除や片付けに時間をかけたくない、自分が</span><span class="linebrk">リフレッシュする時間を大切にしたい、そうした思いをいつもお持ちではありませんか？</span><span class="linebrk">CLIKAは、お客様がご自分のための時間を大切にできるよう、</span><span class="linebrk">心を込めて家事サービスをご提供いたします。</span></p><div class="margin-small" style="clear:both;"></div>
						<p class="bold">■インターネット申込みは24時間受付中</p><div class="margin-small"></div>
						<div class="margin-small"></div>
						<span class="star-burst-nursing"><span><span><span><p><a class="text-stroke-shadow white bold">料金<br>1回利用<br>1回2時間</a><p class="text-stroke-flat yellow bold" style="font-size: 25px;">4,000円</p></p></span></span></span></span>
						<div class="button-action"><a class="btn btn-lg btn-primary" href="#calendar">サービス仮申し込み <i class="fa fa-chevron-circle-down"></i></a></div>
					</div>
					<div class="tcol-md-3 tcol-sm-3 tcol-xs-6 tcol-ss-12 serice-content">
						<div class="blog_item grid_item">
							<div class="imghoverclass"> 
									<img src="<?php echo bloginfo('template_directory');?>/assets/img/washing.jpg" alt="洗濯" class="iconhover" style="display:block;">
							</div> 
							<div class="housekeeping-box">
								<header>
									<h5 class="entry-title">家事代行サービス</h5>
									
								</header>
								<div class="entry-content">
									<p>家事代行サービスは、留学生がお客様のご自宅にお伺いし、お掃除や片付け、洗濯等々、日常的な家事をお客様の代わりに行うサービスです</p>
									<p>お客様お一人おひとりのライフスタイルやご事情に合わせて様々なサービスをご用意しております</p>
								</div> 
							</div> 
						</div>
					</div>
					<div class="tcol-md-3 tcol-sm-3 tcol-xs-6 tcol-ss-12 serice-content">
						<div class="post-143 post type-post status-publish format-standard has-post-thumbnail hentry category-photos tag-carousel tag-gallery tag-photos blog_item grid_item">
							<div class="imghoverclass"> 
								
									<img src="<?php echo bloginfo('template_directory');?>/assets/img/kitchen.jpg" alt="Image Gallery Post" class="iconhover" style="display:block;"> 
								
							</div> 
						<div class="housekeeping-box">
							<header><h5 class="entry-title">どなたでもご利用いただけます</h5></header>
							<div class="entry-content">
								<p>仕事や付き合いで、掃除・洗濯等をする時間がない</p>
								<p>共働きで忙しくなかなか家族で出かけられない</p>
								<p>無駄な事はアウトソーシングした方が良い<br>と思ってる</p>
								<p>残業で毎日忙しくて家事が疎かになっている</p>
							</div> 
						</div>
						</div>
					</div>
					<!--<div class="tcol-md-3 tcol-sm-3 tcol-xs-6 tcol-ss-12 serice-content">
						<div class="post-106 post type-post status-publish format-standard has-post-thumbnail hentry category-adventure tag-adventure tag-fun tag-photos blog_item grid_item">
							<div class="imghoverclass"> 
								<img src="<?php echo bloginfo('template_directory');?>/assets/img/bathroom.jpg" alt="Outdoor Adventure" class="iconhover" style="display:block;"> 
							</div> 
						<div class="housekeeping-box">
							<header>
								<h5 class="entry-title">素早い対応</h5>
									
							</header>
							<div class="entry-content">
								<p>お客様のご自宅にお伺いをさせて頂き、急な対応が出来るようスタッフを待機させて頂いております。私達は、「素早い対応」をモットーに安心・安全をお届けして、お客様と心と心のつながりを大切に日々前進しています。</p>
							</div> 
						</div>
						<span class="star-burst"><span><span><span><p><a class="text-stroke-shadow white bold">料金<br>1回利用<br>1回2時間</a><p class="text-stroke-flat yellow bold" style="font-size: 25px;">4,000円</p></p></span></span></span></span>
						</div>
					</div>-->
					
				</div>
			</div>
		
		</div>
	</div>
</div>
</section>

<div class="margin-medium"></div>
    <div id="content" class="container">
   		<div class="row">
     		<div class="main <?php //echo kadence_main_class(); ?>" role="main">
				<?php get_template_part('templates/content', 'page'); ?>
			</div><!-- /.main -->
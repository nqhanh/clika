<?php
/*
Template Name: Career
*/
?>
<style type="text/css">



/* -------------------------------------- */

			ol{
				counter-reset: li;
				list-style: none;
				*list-style: decimal;
				font: 15px 'trebuchet MS', 'lucida sans';
				padding: 0;
				text-shadow: 0 1px 0 rgba(255,255,255,.5);
			}

			ol ol{
				margin: 0 0 0 2em;
			}

			/* -------------------------------------- */

			.rectangle-list a{
				position: relative;
				display: block;
				padding: .4em .4em .4em .8em;
				*padding: .4em;
				margin: .5em 0 .5em 2.5em;
				background: #ddd;
				color: #444;
				text-decoration: none;
				-webkit-transition: all .3s ease-out;
				-moz-transition: all .3s ease-out;
				-ms-transition: all .3s ease-out;
				-o-transition: all .3s ease-out;
				transition: all .3s ease-out;	
			}

			.rectangle-list a:hover{
				background: #eee;
			}	

			.rectangle-list a:before{
				content: counter(li);
				counter-increment: li;
				position: absolute;	
				left: -2.5em;
				top: 50%;
				margin-top: -1em;
				background: #fa8072;
				height: 2em;
				width: 2em;
				line-height: 2em;
				text-align: center;
				font-weight: bold;
			}
			
			.rectangle-list a:after{
				position: absolute;	
				content: '';
				border: .5em solid transparent;
				left: -1em;
				top: 50%;
				margin-top: -.5em;
				-webkit-transition: all .3s ease-out;
				-moz-transition: all .3s ease-out;
				-ms-transition: all .3s ease-out;
				-o-transition: all .3s ease-out;
				transition: all .3s ease-out;				
			}

			.rectangle-list a:hover:after{
				left: -.5em;
				border-left-color: #fa8072;				
			}
</style>

<div class="margin-hu"></div>
<section class="one-overview-top">

<div class="container">
<div class="margin-medium"></div>
<div class="career-page slash-bg">

<div class="col-lg-8 alignleft relative-cls">

			<h1 class="uppercase medium-font text-stroke blue bold visible-lg"><img src="<?php echo bloginfo('template_directory');?>/assets/img/pen-and-paper-md.png" alt="pen-and-paper" />&nbsp;私たちと一緒に働きませんか？</h1>
			<h4 class="text-stroke-flat pink bold"><span class="linebrk">CLIKAでは、お客様の笑顔とともに、働くスタッフの皆さんの笑顔もとても大切にしたいと考えています</span></h4>
			<p></p><img src="<?php echo bloginfo('template_directory');?>/assets/img/icon_check.png" alt="icon_check" class="alignleft" />
			<p><span class="linebrk">今までの経験を生かし、人の役に立てる、そしてお客様からの感謝に「やりがい」を感じることができる、</span><span class="linebrk">そんな素晴らしい仕事を一緒にやってみませんか？</span></p>
			<img src="<?php echo bloginfo('template_directory');?>/assets/img/icon_check.png" alt="icon_check"  class="alignleft" />
			<p><span class="linebrk">働く皆さんの応募のきっかけも「日本の文化を学びたい」、「自分の時間を有効に使いたい」、</span><span class="linebrk">「新しい可能性にチャレンジしたい」など様々です。</span></p>
			<img src="<?php echo bloginfo('template_directory');?>/assets/img/icon_check.png" alt="icon_check" class="alignleft" />
			<p><span class="linebrk">スタッフの方々が安心して仕事をしていただけるよう、研修や日頃のサポートも充実しています。</span></p>
			<img src="<?php echo bloginfo('template_directory');?>/assets/img/icon_check.png" alt="icon_check" class="alignleft" />
			<p><span class="linebrk">未経験の方でも安心して始められるよう環境を整えていますので、まずはお気軽にお問い合わせください。</span></p>
			
			
			<div class="career-add"><img src="<?php echo bloginfo('template_directory');?>/assets/img/icon_check.png" alt="icon_check" class="alignleft" />
			<p><span class="linebrk">いつものバイトより時給が高い・給料は現金でもらえる・複雑なマニュアルは無い</span><span class="linebrk">簡単なトレーニングを受けてから仕事を開始・一般的な日本人をもっと知ることができる</span><span class="linebrk">学校以外での日本語力を磨くチャンス・東京都で一番働きやすい事業所</span></p></div>			
			
			</div>
			




	<link rel="stylesheet" type="text/css" href="<?php echo bloginfo('template_directory');?>/assets/css/component.css" />
		<script src="<?php echo bloginfo('template_directory');?>/assets/js/modernizr.custom.js"></script>
<?php $pics = array
		(
			array("text" => "ご応募お待ちしています。","pic" => "/assets/img/welcome.jpg"),
			array("text" => "未経験の方でもOK","pic" => "/assets/img/dhs.jpg"),
			array("text" => "いつものバイトより時給が高い","pic" => "/assets/img/v-family.jpg"),
			array("text" =>"給料は現金でもらえる","pic" => "/assets/img/money.jpg"),
			array("text" =>"複雑なマニュアルは無い簡単なトレーニングを受けてから仕事を開始","pic" => "/assets/img/healthcare.jpg"),
			array("text" =>"一般的な日本人をもっと知ることができる学校以外での日本語能力を磨くチャンス","pic" => "/assets/img/maternity-career.jpg"),
			array("text" =>"東京都で一番働きやすい事業所","pic" => "/assets/img/dhs-2.jpg")
		);
		//shuffle($pics);	
		$picurl = get_bloginfo('template_directory');
		?>
<div class='col-lg-4 career-slide alignright'>
			
			<ul id="elasticstack" class="elasticstack">
				<?php foreach ($pics as $pic => $value) {
						echo "<li><img src='".$picurl.$pics[$pic]['pic']."' width='300px' height='260px' alt='".$pics[$pic]['text']."' /><h5>".$pics[$pic]['text']."</h5></li>";
				}?>				
			</ul>
			
</div>		
		<script src="<?php echo bloginfo('template_directory');?>/assets/js/draggabilly.pkgd.min.js"></script>
		<script src="<?php echo bloginfo('template_directory');?>/assets/js/elastiStack.js"></script>
		<script>
			new ElastiStack( document.getElementById( 'elasticstack' ) );
		</script>




</div></div>
</section>	
<div class="margin-medium"></div>
    <div id="content" class="container">

		<div class="row">				

				<div class="col-lg-6">
				
					<div class="career-home" style="margin-top: -10px;">
					<h1 class="lh-60 medium-font text-right text-stroke-flat yellow bold career-h1-margin">ご応募お待ちしています。</h1>
					<h3 class="text-stroke-flat white bold bluebg">お仕事開始までの流れ</h3>
						<div class="qa-careerbg">
							<p>まずは、ホームページ上の「応募フォーム」にご入力いただくか、お電話にてお問い合わせください。担当のコーディネーターから仕事の内容について説明をします。</p>
							<p>その上で、面接日を設定、必要なときは書類選考となります。</p>							
						</div>
						<div id="qa-chevron"></div>	
						
						<div class="qa-careerbg">
							<p><span class="linebrk">面接には、履歴書（写真添付）、職務経歴書、及び現住所を確認できるもの（健康保険証、</span><span class="linebrk">もしくは、運転免許証等）を持参して下さい。</span></p>
							<p>面接では、会社概要、仕事の内容、雇用条件等を説明いたします。</p>							
						</div>
						<div id="qa-chevron"></div>
						
						<div class="qa-careerbg">
							<p>面接に合格された方には、オリエンテーションに参加していただきます。</p>
							<p><span class="linebrk">ここでは、お仕事を開始する前の注意点や、個人情報保護などのコンプライアンス、</span><span class="linebrk">>マナー、実地研修が入ります。また、業務の手順、</span><span class="linebrk">勤務報告などについての説明を行います。尚、オリエンテーションは、</span><span class="linebrk">2次選考も兼ねています。</span></p>							
						</div>
						<div id="qa-chevron"></div>
						
						<div class="qa-careerbg">
							<p>オリエンテーションを無事に修了した方には、会社から本登録のご連絡をいたします。</p>							
						</div>
						<div id="qa-chevron"></div>
						
						<div class="qa-careerbg">
							<p>本登録を終えた方には、随時、お仕事のご案内をいたします。</p>
							<p>最初の顧客宅でのお仕事を開始する前に、スタッフの方へは紹介状等をお渡しします。</p>							
						</div>
						<div id="qa-chevron"></div>
						
						<div class="qa-careerbg">
							<p>コーディネーターが、随時、お仕事を案内し、業務内容の説明とスケジュールの調整を行います。お仕事は、新しい顧客ごとに「業務依頼書」等により指示します。</p>
						</div><div class="qa-square">&nbsp;</div>
						<div id="qa-chevron"></div>	
						
					</div>
					<div class="margin-small"></div>
					
					<p align="center"><a href="../application-form/"><img src="<?php echo bloginfo('template_directory');?>/assets/img/apply.gif"></a></p>	
					<div class="margin-medium"></div>
				<div class="contact-border-career career-slide slash-pink-bg">				
						<img src="../wp-content/themes/virtue/assets/img/contactus.png" alt="" class="img-responsive center-block alignleft" width="100px" style="margin-right:20px;">
						<p>当社へご意見・ご質問等ありましたらこちらからお問合せください。</p>
						<p>また、 FAQ も併せてご覧ください。</p>
						<h3 class="red bold alignleft" style="color:#df4443;font-weight:700;margin-right: 20px;"><i class="fa fa-phone"></i> 03-5159-1212</h3><a class="btn btn-lg btn-pink" href="../answers/">よくあるご質問</a> 
				</div>
				</div>
				
				<div class="col-lg-6 career-slide">
				<fieldset class="career-form-fieldset">
					<legend class="career-form-legend"><h2 class="text-stroke-flat white bold">応募フォーム</h2></legend>
					<div class="career-form">
						<?php 
						//echo do_shortcode('[contact-form-7 id="853" title="無題"]');
						echo do_shortcode('[contact-form-7 id="846" title="応募フォーム"]');?>
					</div>
				</fieldset>
				</div>
</div>
				
				
		
   		<div class="row">
     		<div class="main <?php //echo kadence_main_class(); ?>" role="main">
				<?php //get_template_part('templates/content', 'page'); ?>
			</div><!-- /.main -->
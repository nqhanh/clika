
<header class="banner headerclass" role="banner">
<?php 
//detect mobile agent
if ( wp_is_mobile() ) {
    // any mobile platform
    echo "<style>#fixed {position: relative;}</style>";
} else echo '<script src="http://code.jquery.com/jquery-latest.js"></script>';
?>

<?php if (kadence_display_topbar()) : ?>
  <section id="topbar" class="topclass large">
    <div class="container">
      <div class="row">
        <div class="col-md-6 col-sm-6 topbarmenu kad-topbar-left">
          <div class="topbarmenu clearfix">
          <?php /*if (has_nav_menu('topbar_navigation')) :
              wp_nav_menu(array('theme_location' => 'topbar_navigation', 'menu_class' => 'sf-menu'));
            endif;*/?>

<?php 

	if (is_user_logged_in()) { 
?>
    <!-- Logout link on top -->	
	<?php if (has_nav_menu('topbar_navigation')) :
              wp_nav_menu(array('theme_location' => 'topbar_navigation', 'menu_class' => 'sf-menu'));
            endif;?>
    <!-- / top -->
<?php
	// Else if user is not logged in, show login and register forms
	} else {
?>
    <!-- Login / Register links on top -->
		<ul class="login" <?php if (!get_option('users_can_register')) { ?> style="width:150px;"<?php } ?>>
	    	<!-- Login / Register -->
			<li><a id="login" href="#"><?php _e('Log In'); ?></a></li>           
		</ul>
    <!-- / top -->
<?php } ?>
<!--END panel -->
<!-- Login Form -->
<div id="loginpanel" <?php if (get_option('users_can_register')) { ?>style="display:none;"<?php }else{ ?>style="display:none;right:0;"<?php } ?>>
	<div class="content">
				<form class="clearfix" action="<?php echo site_url('wp-login.php') ?>" method="post">
					<label class="grey" for="log"><?php _e('Username') ?>:</label>
					<input class="field" type="text" name="log" id="log" value="<?php echo wp_specialchars(stripslashes($user_login), 1) ?>" size="23" />
					<label class="grey" for="pwd"><?php _e('Password:') ?></label>
					<input class="field" type="password" name="pwd" id="pwd" size="23" />
	            	<label><input name="rememberme" id="rememberme" type="checkbox" checked="checked" value="forever" />&nbsp;<?php _e('Remember Me') ?></label>
        			<div class="clear"></div>
					<input type="submit" name="submit" value="<?php _e('Log In') ?>" class="bt_login" />
					<input type="hidden" name="redirect_to" value="<?php echo $_SERVER['REQUEST_URI']; ?>"/>
					<a class="lost-pwd" href="<?php echo site_url(); ?>/my-account/lost-password/"><?php _e('Lost Password') ?></a>
				</form>
				<?php if (get_option('users_can_register')) { ?>
					<a href="<?php echo site_url(); ?>/my-account/"><?php _e('会員登録 （無料）'); ?></a>
            	<?php } ?>
				<p><a href="<?php echo site_url()?>/wp-login.php?loginFacebook=1&redirect=<?php echo ( is_ssl() ? 'https' : 'http' ) . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']?>" onclick="window.location = '<?php echo site_url()?>/wp-login.php?loginFacebook=1&redirect='+window.location.href; return false;" class="btn btn-facebook-sliding"><i class="fa fa-facebook" style="font-size:25px;"></i>&nbsp;&nbsp;&nbsp;ログイン</a></p>
	</div>
</div>
<!-- /login -->

			
			
			
            <?php if(kadence_display_topbar_icons()) : ?>
            <div class="topbar_social">
              <ul>
                <?php global $virtue; $top_icons = $virtue['topbar_icon_menu'];
                foreach ($top_icons as $top_icon) {
                  if(!empty($top_icon['target']) && $top_icon['target'] == 1) {$target = '_blank';} else {$target = '_self';}
                  echo '<li><a href="'.$top_icon['link'].'" target="'.$target.'" title="'.esc_attr($top_icon['title']).'" data-toggle="tooltip" data-placement="bottom" data-original-title="'.esc_attr($top_icon['title']).'">';
                  if($top_icon['url'] != '') echo '<img src="'.$top_icon['url'].'"/>' ; else echo '<i class="'.$top_icon['icon_o'].'"></i>';
                  echo '</a></li>';
                } ?>
              </ul>
            </div>
          <?php endif; ?>
            <?php global $virtue; if(isset($virtue['show_cartcount'])) {
               if($virtue['show_cartcount'] == '1') { 
                if (class_exists('woocommerce')) {
                  global $woocommerce; ?>
                    <ul class="kad-cart-total">
                      <li>
                      <a class="cart-contents" href="<?php echo $woocommerce->cart->get_cart_url(); ?>" title="<?php esc_attr_e('View your shopping cart', 'woocommerce'); ?>">
                          <i class="icon-shopping-cart" style="padding-right:5px;"></i> <?php _e('Your Cart', 'virtue');?> <span class="kad-cart-dash">-</span> <?php echo $woocommerce->cart->get_cart_total(); ?>
                      </a>
                    </li>
                  </ul>
                <?php } } }?>
          </div>
        </div><!-- close col-md-6 --> 
        <div class="col-md-6 col-sm-6 kad-topbar-right">
          <div id="topbar-search" class="topbar-widget">
            <?php if(kadence_display_topbar_widget()) { if(is_active_sidebar('topbarright')) { dynamic_sidebar(__('Topbar Widget', 'virtue')); } 
              } else { if(kadence_display_top_search()) {get_search_form();} 
          } ?>
        </div>
        </div> <!-- close col-md-6-->
      </div> <!-- Close Row -->
    </div> <!-- Close Container -->
  </section>
<?php endif; ?>
<?php global $virtue; if(isset($virtue['logo_layout'])) {
  if($virtue['logo_layout'] == 'logocenter') {$logocclass = 'col-md-12'; $menulclass = 'col-md-12';}
  else if($virtue['logo_layout'] == 'logohalf') {$logocclass = 'col-md-6'; $menulclass = 'col-md-6';}
  else {$logocclass = 'col-md-2'; $menulclass = 'col-md-10';} 
} else {$logocclass = 'col-md-2'; $menulclass = 'col-md-10'; }?>
<section id="section1" class="large">
	<div class="container">
		<div class="row">
			<div class="top-banner">
				<div class="alignright top-contact">
					<p>まずはご相談ください  (土・日・祝日除く） 9:00〜18:00</p><h2 class="red bold" style="color:#df4443;font-weight:700;margin: 0;font-size: 36px;"><i class="icon-phone"></i> 03-5159-1212</h2>
					<div class="homepage-one-action alignright"><!--<a href="<?php echo site_url();?>/contact-us/#pageheader"><i class="icon-map-marker"></i>&nbsp;こちらにいらっしゃい</a><a href="<?php echo site_url();?>/contact-us/#content" style="float:right;"><i class="icon-envelope"></i>&nbsp;お問い合わせ</a>--><a class="top-add">本社所在地 〒104-0061 東京都中央区銀座4-5-1</a>
					</div>
				</div>
				
				<div class="alignright top-intro prices">
					<a href="<?php echo site_url();?>/#two-services"><h3 class="custom-h3"><span class="blue bold">料金 1回利用 1回2時間</span><span class="text-stroke-flat yellow bold"> 4,000円</span></h3>
					<h4 class="custom-h4"><span class="text-stroke-flat white bold">インターネット申込みは24時間受付中</span></h4></a>
				</div>
				<div class="alignright top-intro logo-aline">
					<a href="http://aline.jp" target="_blank"><img src="<?php echo bloginfo('template_directory');?>/assets/img/logo-aline.png" width="190px" alt="aline"></a>
					
				</div>
				<div class="alignright <?php //echo $logocclass; ?> col-md-2  clearfix kad-header-left abso">
					<div id="logo" class="logocase">
					  <a class="brand logofont" href="<?php echo home_url(); ?>/">
							  <?php global $virtue; if (!empty($virtue['x1_virtue_logo_upload']['url'])) { ?> <div id="thelogo"><img src="<?php echo $virtue['x1_virtue_logo_upload']['url']; ?>" alt="<?php  bloginfo('name');?>" class="kad-standard-logo" />
								 <?php if(!empty($virtue['x2_virtue_logo_upload']['url'])) {?> <img src="<?php echo $virtue['x2_virtue_logo_upload']['url'];?>" class="kad-retina-logo" style="max-height:<?php echo $virtue['x1_virtue_logo_upload']['height'];?>px" /> <?php } ?>
								</div> <?php } else { bloginfo('name'); } ?>
								</a>
					  <?php if (isset($virtue['logo_below_text'])) { ?> <p class="kad_tagline belowlogo-text"><?php echo $virtue['logo_below_text']; ?></p> <?php }?>
					</div> <!-- Close #logo -->
				</div><!-- close logo span -->
				
			</div>
		</div>
	</div>
</section>
<section id="fixed" class="large">
  <div class="container">
    <div class="row">
       <div class="<?php echo $logocclass; ?>  clearfix kad-header-left" style="position: absolute;">
            <div id="logo-nav" class="logocase large">
              <a class="brand logofont" href="<?php echo home_url(); ?>/">
                      <?php global $virtue; if (!empty($virtue['x1_virtue_logo_upload']['url'])) { ?> <div id="thelogo"><img src="<?php echo $virtue['x1_virtue_logo_upload']['url']; ?>" alt="<?php  bloginfo('name');?>" class="kad-standard-logo" />
                         <?php if(!empty($virtue['x2_virtue_logo_upload']['url'])) {?> <img src="<?php echo $virtue['x2_virtue_logo_upload']['url'];?>" class="kad-retina-logo" style="max-height:<?php echo $virtue['x1_virtue_logo_upload']['height'];?>px" /> <?php } ?>
                        </div> <?php } else { bloginfo('name'); } ?>
                        </a>
              <?php if (isset($virtue['logo_below_text'])) { ?> <p class="kad_tagline belowlogo-text"><?php echo $virtue['logo_below_text']; ?></p> <?php }?>
           </div> <!-- Close #logo -->
       </div><!-- close logo span -->

       <div class="<?php echo $menulclass; ?> kad-header-right" style="width:100%;">
         <nav id="nav-main" class="clearfix" role="navigation" style="margin: 0;">
          <?php
            if (has_nav_menu('primary_navigation')) :
              wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'sf-menu')); 
            endif;
           ?>
         </nav> 
        </div><!-- Close span7 -->       
    </div> <!-- Close Row -->
    <?php if (has_nav_menu('mobile_navigation')) : ?>
           <div id="mobile-nav-trigger" class="nav-trigger">
              <a class="nav-trigger-case mobileclass collapsed" rel="nofollow" data-toggle="collapse" data-target=".kad-nav-collapse">
                <div class="kad-navbtn"><i class="icon-reorder"></i></div>
                <div class="kad-menu-name"><?php echo __('clika', 'virtue'); ?></div>
              </a>
            </div>
            <div id="kad-mobile-nav" class="kad-mobile-nav">
              <div class="kad-nav-inner mobileclass">
                <div class="kad-nav-collapse">
                 <?php wp_nav_menu( array('theme_location' => 'mobile_navigation','items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>', 'menu_class' => 'kad-mnav')); ?>
               </div>
            </div>
          </div>   
          <?php  endif; ?> 
  </div></section> <!-- Close Container -->
  <?php
            if (has_nav_menu('secondary_navigation')) : ?>
  <section id="cat_nav" class="navclass">
    <div class="container">
     <nav id="nav-second" class="clearfix" role="navigation">
     <?php wp_nav_menu(array('theme_location' => 'secondary_navigation', 'menu_class' => 'sf-menu')); ?>
   </nav>
    </div><!--close container-->
    </section>
    <?php endif; ?> 
     <?php global $virtue; if (!empty($virtue['virtue_banner_upload']['url'])) { ?> <div class="container"><div class="virtue_banner"><img src="<?php echo $virtue['virtue_banner_upload']['url']; ?>" /></div></div> <?php } ?>		
</header>
<div class="margin-huge"></div>

		<script type="text/javascript">
		var nav = $.noConflict(true);
		nav(document).on("scroll",function(){
			if(nav(document).scrollTop()>1){ 
				nav("#fixed").removeClass("large").addClass("small");
				nav("#topbar").removeClass("large").addClass("small");
				nav("#section1").removeClass("large").addClass("small");
				nav("#logo-nav").removeClass("large").addClass("small");
				
				}
			else{
				nav("#fixed").removeClass("small").addClass("large");
				nav("#topbar").removeClass("small").addClass("large");
				nav("#section1").removeClass("small").addClass("large");
				nav("#logo-nav").removeClass("small").addClass("large");
				
				}
			});
	</script>	

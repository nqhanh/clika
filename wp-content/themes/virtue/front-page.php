<div class="margin-medium-minus"></div><section class="one-overview-unique">
	<div class="container">
		<div class="row">								
			</div>
				<div class="margin-huge"></div>
					<!--<div class="front-col-xs-7">					
					<h1 class="medium-font text-stroke-blur lightblue bold">ハウスキーピング・介護サービス</h1>
					<h5 class="text-stroke-blur lightblue"><span class="linebrk">私たちがサービスを提供することによって、ご家庭で笑顔や</span><span class="linebrk">明るい声が絶えず、お客様が毎日の生活を心地よく感じていただけること、</span><span class="linebrk">これが私たちの目標です。</span>
					</h5>
					
					<div class="margin-small"></div><div class="margin-small"></div>
					<div class="button-action" align="center"><a class="btn btn-lg btn-primary" href="<?php echo site_url()?>/contact-us/">お気軽にご相談下さい&nbsp;<i class="fa fa-chevron-right"></i></a></div>
					</div>-->	
				<div class="margin-small"></div><div class="margin-small"></div><div class="margin-small"></div>
				<h1 class="large-font text-stroke-blur lightblue bold" style="text-align:center;">私たちは家事と介護のお手伝いをします。</h1>
				<div class="margin-small"></div><div class="margin-small"></div><div class="margin-small"></div>	
				<div class="margin-huge"></div>
				<div class="margin-small"></div>
			</div>		
	</div>
</section>

<div class="container">
	<div class="row">	
		<section id="two-services" class="one-overview-top"><div class="margin-medium"></div><div class="margin-small"></div>
			<div class="container">
				<div class="row">	
					<div class="col-xs-4 notebox">	
					<h5 class="blue bold"><i class="fa fa-map-marker"></i> サービス対象エリア</h5>
						<div class="h-20"></div>
						<p>現在、東京２３区内にてサービスを提供致します。</p>
						
					</div>
					<div class="col-xs-4 notebox">
						<h5 class="blue bold">ハウスキーピング（家事代行）サービス</h5>
						<p><span class="linebrk">CLIKAは、お客様がご自分のための時間を大切にできる</span><span class="linebrk">よう、心を込めて家事サービスをご提供いたします。</span></p>
						<p><a class="btn btn-lg-home btn-primary" href="<?php echo site_url()?>/housekeeping/#calendar">サービス仮申し込み <i class="fa fa-chevron-right"></i> </a></p>
					</div>
					<div class="col-xs-4 notebox">
						<h5 class="blue bold">訪問介護（ホームヘルプ）</h5>
						<p><span class="linebrk">ご家族の介護負担を軽減する事が</span>私たちの目標です。</p>
						<p><a class="btn btn-lg-home btn-primary" href="<?php echo site_url()?>/nursing-care/#calendar">サービス仮申し込み <i class="fa fa-chevron-right"></i> </a></p>
					</div>
				</div>
			</div>
		</section>	
		<div class="margin-medium"></div>
		<div class="col-xs-3">&nbsp;</div><div class="col-xs-6" style="border-bottom:1px dashed #e2e2e2;">&nbsp;</div><div class="col-xs-3">&nbsp;</div>
		<div class="margin-medium" style="clear:both;"></div>
		<div class="col-xs-1">
		</div>
		<div class="col-xs-10">
		<h2 class="blue bold staff-title" style="text-align:center;">スタッフ</h2>
			<!--<p class="slash-bg" style="padding: 10px;"><span class="linebrk">スタッフは、お客様がお部屋で快適に過ごせるように気持ちを込めてサービスを行なっています。</span>
			<span class="linebrk">また、コーディネーターはお客様からのきめ細かいご要望をヒアリングして、スタッフと円滑にコミュニケーションを取ってお客様にご満足</span>
			<span class="linebrk">いただけるように動いています。</span></p>-->
		</div>
		<div class="col-xs-1">
		</div>
		<div style="clear:both;"></div>
		<div class="col-xs-1">
		</div>
		<div class="col-xs-10">
		
					<div class="row">
						<!--<h5 class="blue bold" style="text-align:center;">是非、安心してClikaのサービスをお試し下さい。</h5>-->
						
											
						<?php 
							query_posts(array( 
								'post_type' => 'portfolio',
								'orderby' => 'rand',
								'showposts' => 3 
							) );  
						?>
						<?php while (have_posts()) : the_post(); ?><div class="home-staffs">
								<a href="<?php the_permalink() ?>"><?php the_post_thumbnail(array(65, 65),  array('title'=> trim(strip_tags( $attachment->the_title)))); ?>
								<?php the_title(); ?></a>
								<p><?php echo get_the_excerpt(); ?></p></div>
						<?php endwhile;?>
					</div>		
					
		</div>
		<div class="col-xs-1">
		</div>
	</div>
</div>
<div class="margin-huge"></div>
<section class="slash-bg"><div id="white-chevron"></div>
	<div class="container">
		<div class="row"><div class="margin-small"></div>				
				<div class="col-lg-8">
				<div class="top_above_footer_h5_box">
				<h5><i class="medium-font fa fa-info-circle blue"></i> CLIKAについて</h5>
				</div>
					<div>
						<!--<h2 class="text-stroke-flat blue bold">我々は？</h2>-->
						<p></p>
						<span class="bottomlinebrk">CLIKAとは、短時間の労働を希望する留学生と安価に家事介護の人手を必要としているご家庭を結びつけるWebサービスです。CLIKA.jp web サービスはA-LINE Vietnamで企画開発運営され、A-LINE日本本社は留学生向け家事介護トレーニングや</span><span class="bottomlinebrk">カスタマーリレーションを担当します。</span><div style="clear:both;"></div><p></p>

						<p>従来の家事介護サービスは非常に高価なため一般家庭にとって使い勝手の良いサービスとは言えませんでしたが、新たに開始</span><span class="bottomlinebrk">するCLIKA.jpでは従来価格の半額程度でサービスを提供することで一般家庭への普及を目指します。</p>

				<div class="margin-small"></div><div style="clear:both;"></div>
					</div>				
				</div>
				<div class="col-lg-4">
				<div class="top_above_footer_h5_box">
				<h5><i class="medium-font fa fa-question-circle blue"></i> CLIKAを選ぶ理由</h5>
				</div><p></p>				
				<p class="bold">１．留学生を活用した安価な家事介護補助サービス</p>
				<span class="bottomlinebrk">CLIKAには、スタッフの皆が若くて明るくて、日本の習慣も言葉も理解できる留学生です。</span><div style="clear:both;"></div><p></p>
				<p class="bold">2．インターネットで24時間受付中</p>
				<p>忙しい方にも簡単に使用できます。</p>
				</div>
			</div><div class="margin-small"></div>
				
			</div>		
	
	</div>
</section>


			

<?php
if(isset($virtue['homepage_layout']['enabled'])){
		$i = 0;
		$show_pagetitle = false;
		foreach ($virtue['homepage_layout']['enabled'] as $key=>$value) {
			if($key == "block_one") {
				$show_pagetitle = true;
			}
			$i++;
			if($i==2) break;
		}
	} 
	if($show_pagetitle == true) {
		?><div id="homeheader" class="welcomeclass">
								<div class="container">
									<?php get_template_part('templates/page', 'header'); ?>
								</div>
							</div><!--titleclass-->
	<?php } ?>
	
    <div id="content" class="container homepagecontent">
   		<div class="row">
          <div class="main <?php echo kadence_main_class(); ?>" role="main">

      	<?php if(isset($virtue['homepage_layout']['enabled'])) { $layout = $virtue['homepage_layout']['enabled']; } else {$layout = array("block_twenty" => "block_twenty", "block_five" => "block_five"); }

				if ($layout):

				foreach ($layout as $key=>$value) {

				    switch($key) {

				    	case 'block_one':?>
						   <?php if($show_pagetitle == false) {?>
				    	  <div id="homeheader" class="welcomeclass">
									<?php get_template_part('templates/page', 'header'); ?>
							</div><!--titleclass-->
							<?php }?>
					    <?php 
					    break;
						case 'block_four': ?>
							<?php if(is_home()) { ?>
							<?php if(kadence_display_sidebar()) {$display_sidebar = true; $fullclass = '';} else {$display_sidebar = false; $fullclass = 'fullwidth';} ?>
							<?php global $virtue; if(isset($virtue['home_post_summery']) and ($virtue['home_post_summery'] == 'full')) {$summery = "full"; $postclass = "single-article fullpost";} else {$summery = "summery"; $postclass = "postlist";} ?>
								<div class="homecontent <?php echo $fullclass; ?>  <?php echo $postclass; ?> clearfix home-margin"> 
							<?php while (have_posts()) : the_post(); ?>
							  <?php  if($summery == 'full') {
											if($display_sidebar){
												get_template_part('templates/content', 'fullpost'); 
											} else {
												get_template_part('templates/content', 'fullpostfull');
											}
									} else {
											if($display_sidebar){
											 	get_template_part('templates/content', get_post_format()); 
											 } else {
											 	get_template_part('templates/content', 'fullwidth');
											 }
									}?>
							<?php endwhile; ?>
							<?php if ($wp_query->max_num_pages > 1) : ?>
						        <?php $bignumber = 999999999;
						        $pagargs = array(
						          'base' => str_replace( $bignumber, '%#%', esc_url( get_pagenum_link( $bignumber ) ) ),
						          'format'       => '?page=%#%',
						          'total' => $wp_query->max_num_pages,
						          'current' => max( 1, get_query_var('paged') ),
						          'prev_next'    => True,
						          'prev_text'    => '«',
						          'next_text'    => '»',
						          'type'         => 'plain',
						        ); ?>
						        <div class="wp-pagenavi">
						        <?php echo paginate_links( $pagargs ); ?>
						        </div>
						        
						        <?php endif; ?>
							</div> 
						<?php } else { ?>
						<div class="homecontent clearfix home-margin"> 
							<?php get_template_part('templates/content', 'page'); ?>
						</div>
						<?php 	}
						break;
						case 'block_five':
								get_template_part('templates/home/blog', 'home'); 
						break;
						case 'block_six':
								get_template_part('templates/home/portfolio', 'carousel');		 
						break; 
						case 'block_seven':
								get_template_part('templates/home/icon', 'menu');		 
						break;
						case 'block_twenty':
								get_template_part('templates/home/icon', 'menumock');		 
						break;  
					    }

}
endif; ?>   



<?php //get_template_part('templates/home/about', 'home'); ?>
<?php //get_template_part('templates/home/introduce', 'home'); ?>

</div><!-- /.main -->
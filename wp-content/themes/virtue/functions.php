<?php
ob_start();
/*-----------------------------------------------------------------------------------*/
/* Include Theme Functions */
/*-----------------------------------------------------------------------------------*/

function virtue_lang_setup() {
	load_theme_textdomain('virtue', get_template_directory() . '/languages');
}
add_action( 'after_setup_theme', 'virtue_lang_setup' );
require_once locate_template('/themeoptions/options/virtue_extension.php'); // Options framework extension
require_once locate_template('/themeoptions/framework.php');        // Options framework
require_once locate_template('/themeoptions/options.php');     		// Options framework
require_once locate_template('/lib/utils.php');           			// Utility functions
require_once locate_template('/lib/init.php');            			// Initial theme setup and constants
require_once locate_template('/lib/sidebar.php');         			// Sidebar class
require_once locate_template('/lib/config.php');          			// Configuration
require_once locate_template('/lib/cleanup.php');        			// Cleanup
require_once locate_template('/lib/nav.php');            			// Custom nav modifications
require_once locate_template('/lib/metaboxes.php');     			// Custom metaboxes
require_once locate_template('/lib/comments.php');        			// Custom comments modifications
require_once locate_template('/lib/widgets.php');         			// Sidebars and widgets
require_once locate_template('/lib/aq_resizer.php');      			// Resize on the fly
require_once locate_template('/lib/scripts.php');        			// Scripts and stylesheets
require_once locate_template('/lib/custom.php');          			// Custom functions
require_once locate_template('/lib/admin_scripts.php');          	// Icon functions
require_once locate_template('/lib/authorbox.php');         		// Author box
require_once locate_template('/lib/custom-woocommerce.php'); 		// Woocommerce functions
require_once locate_template('/lib/virtuetoolkit-activate.php'); 	// Plugin Activation
require_once locate_template('/lib/custom-css.php'); 			    // Fontend Custom CSS

/* Disable WordPress Admin Bar for all users but admins. */
  show_admin_bar(false);

// Changes link destination of 'Continue Shopping' button on cart page
add_filter( 'woocommerce_continue_shopping_redirect', 'my_custom_continue_redirect' );
function my_custom_continue_redirect( $url ) {
return site_url();
} 
add_filter( 'woocommerce_return_to_shop_redirect', 'my_custom_return_redirect' );
function my_custom_return_redirect( $url ) {
return site_url();
} 

//Khởi tạo function cho shortcode
function progress_bar_shortcode() {
	if(isset($_GET['key'])){
		echo '<div class="checkout-wrap">
		  <ul class="checkout-bar">
			<li class="visited">
			  日付選択
			</li>
			<li class="visited">情報入力</li>
			<li class="visited">お支払い</li>
			<li class="active-last">完了</li>
		  </ul>
		</div>
		<div class="wc-class"></div>';
	}
	else {
		echo '<div class="checkout-wrap">
		  <ul class="checkout-bar">
			<li class="visited first">
			  日付選択
			</li>
			<li class="active">情報入力</li>
			<li class="next">お支払い</li>
			<li class="">完了</li>
		  </ul>
		</div>
		<div class="wc-class"></div>';
	}
}
add_shortcode( 'progress_bar', 'progress_bar_shortcode' );

//Add new login by facebook button
add_action( 'woocommerce_login_form_end', 'fb_login_button' );
function fb_login_button() {
    if ( get_option( 'woocommerce_enable_myaccount_registration' ) == 'yes' ) {
	?>
	<div class="margin-small"></div>
	<p>■Facebookアカウントでのログイン</p>
	<p class="form-row">
		<a href="<?php echo site_url()?>/wp-login.php?loginFacebook=1&redirect=<?php echo ( is_ssl() ? 'https' : 'http' ) . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI']?>" onclick="window.location = '<?php echo site_url()?>/wp-login.php?loginFacebook=1&redirect='+window.location.href; return false;" class="btn btn-facebook"><i class="fa fa-facebook" style="font-size:25px;"></i>&nbsp;&nbsp;&nbsp;でログインする</a>
	</p>
	<?php
	}
}

// custom admin login logo
function custom_login_logo() {
	echo '<style type="text/css">
	h1 a { background-image: url('.get_bloginfo('template_directory').'/assets/img/clika-logo.gif) !important;webkit-background-size: 249px !important;background-size: 249px !important;height: 69px !important;width: 249px !important; }
	</style>';
}
add_action('login_head', 'custom_login_logo');

// custom admin login header link & alt text
function change_wp_login_url() {
return site_url(); //or echo your own url
}
function change_wp_login_title() {
return get_option('blogname'); // or echo your own alt text
}
add_filter('login_headerurl', 'change_wp_login_url');
add_filter('login_headertitle', 'change_wp_login_title');

add_action( 'user_register', 'clika_user_register', 10, 1 );
function clika_user_register( $user_id )
{
    $user = get_user_by( 'id', $user_id );

    $blogname = wp_specialchars_decode( get_option( 'blogname' ), ENT_QUOTES );

    $message  = sprintf( __( 'New user registration on your site %s:' ), $blogname ) . "\r\n\r\n";
    $message .= sprintf( __( 'Username: %s'), $user->user_login ) . "\r\n\r\n";
    $message .= sprintf( __( 'E-mail: %s'), $user->user_email ) . "\r\n";
	
	$headers[] = 'Cc: Yohan Matsutani <matsutani@aline.jp>'; //hanhdo

    @wp_mail( get_option( 'admin_email' ), sprintf( __( '[%s] New User Registration' ), $blogname ), $message, $headers);
}

/** 
 * Manipulate default state and countries
 */
add_filter( 'default_checkout_country', 'change_default_checkout_country' );
add_filter( 'default_checkout_state', 'change_default_checkout_state' );
 
function change_default_checkout_country() {
  return 'JP'; // country code
}
 
function change_default_checkout_state() {
  return 'JP13'; // state code
}
?>



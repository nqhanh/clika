<?php
/*
Template Name: Myaccount 
* @author hanhdo
*/
$whichpage = $_SERVER[REQUEST_URI];
if (strpos($whichpage,'lost-password') !== false) {
    $pagetitle = "パスワードのリセット";
}
else $pagetitle = "マイアカウント";
?>


<div class="container">
<div class="margin-medium"></div>
<div><h1 class="medium-font uppercase text-stroke blue bold"><?php echo $pagetitle;?></h1>
</div>
</div>

    <div id="content" class="container">
   		<div class="row">
     		<div class="main <?php echo kadence_main_class(); ?>" role="main">
				<?php get_template_part('templates/content', 'page'); ?>
			</div><!-- /.main -->
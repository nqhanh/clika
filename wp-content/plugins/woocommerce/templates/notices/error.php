<?php
/**
 * Show error messages
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) exit; // Exit if accessed directly

if ( ! $messages ) return;
if ( wp_is_mobile() ) {
?>
<ul class="woocommerce-error">	
	<?php foreach ( $messages as $message ) : ?>
		<li><?php echo wp_kses_post( $message ); ?></li>
	<?php endforeach; ?>
</ul>
<?php } else {?>
<script src='//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js'></script>
<script>
var jpopup = jQuery.noConflict();
jpopup(document).ready(function () {
    jpopup('#hover').click(function () {
        jpopup(this).fadeOut();
        jpopup('#popup').fadeOut();
    });
    jpopup('#close').click(function () {
        jpopup('#hover').fadeOut();
        jpopup('#popup').fadeOut();
    });
});
//@ sourceURL=pen.js
</script>
<ul id="popup" class="woocommerce-error slash-pink-bg">
	<div id="close"><i class="fa fa-times"></i></div>
	<?php foreach ( $messages as $message ) : ?>
		<li><?php echo wp_kses_post( $message ); ?></li>
	<?php endforeach; ?>
</ul><div style="clear:both;"></div>
<?php } ?>
<?php
/**
 * Japan States
 *
 * @author      WooThemes
 * @category    i18n
 * @package     WooCommerce/i18n
 * @version     2.0.0
 */
global $states;

$states['JP'] = array(
	'JP13' => __( 'Tokyo', 'woocommerce' ),
);

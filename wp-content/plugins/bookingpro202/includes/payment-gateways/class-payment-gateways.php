<?php

/*
* Title                   : Booking System PRO (WordPress Plugin)
* Version                 : 2.0
* File                    : includes/payment-gateways/class-payment-gateways.php
* File Version            : 1.0.2
* Created / Last Modified : 03 December 2014
* Author                  : Dot on Paper
* Copyright               : © 2012 Dot on Paper
* Website                 : http://www.dotonpaper.net
* Description             : Booking System PRO payment gateways PHP class.
*/

    if (!class_exists('DOPBSPPaymentGateways')){
        class DOPBSPPaymentGateways{
            /*
             * Private variables.
             */
            private $payment_gateways = array();
            
            /*
             * Public variables.
             */
            public $paypal;
            
            /*
             * Constructor
             */
            function DOPBSPPaymentGateways(){
                array_push($this->payment_gateways, array('class' => 'DOPBSPPayPal',
                                                          'id' => 'paypal'));
                
                $this->payment_gateways = apply_filters('dopbsp_filter_payment_gateways', $this->payment_gateways);
                
                $this->init($this->payment_gateways);
            }
            
            /*
             * Set payment gateways classes.
             */
            function init($payment_gateways){
                /*
                 * Initialize payment gateways classes.
                 */
//                print_r($payment_gateways);
                
                
                for ($i=0; $i<count($payment_gateways); $i++){
                    if (class_exists($payment_gateways[$i]['class'])){  
                        $this->$payment_gateways[$i]['id'] = new $payment_gateways[$i]['class']();
                    }
                }
            }
            
            function translation($classes){
//                echo 'test';
                
                for ($i=0; $i<count($this->payment_gateways); $i++){
                echo $this->payment_gateways[$i]['classTranslation'];
                    array_push($classes, $this->payment_gateways[$i]['classTranslation']);
                }
                
                return $classes;
            }
            
            /*
             * Get payment gateways.
             * 
             * @return list of payment gateways
             */
            function get(){
                return $this->payment_gateways;
            }
        }
    }
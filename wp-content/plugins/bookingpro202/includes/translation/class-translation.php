<?php

/*
* Title                   : Booking System PRO (WordPress Plugin)
* Version                 : 2.0
* File                    : includes/translation/class-translation.php
* File Version            : 1.1.2
* Created / Last Modified : 07 December 2014
* Author                  : Dot on Paper
* Copyright               : © 2012 Dot on Paper
* Website                 : http://www.dotonpaper.net
* Description             : Booking System PRO translation PHP class.
*/

    if (!class_exists('DOPBSPTranslation')){
        class DOPBSPTranslation{
            /*
             * Public variables.
             */
            public $classes = array();
            public $text = array();
            
            /*
             * Constructor
             */
            function DOPBSPTranslation(){
                $this->setClasses();
                
                add_action('init', array(&$this, 'init'));
            }
            
            /*
             * Prints out the translation page.
             * 
             * @return translation HTML page
             */
            function view(){
                global $DOPBSP;
                
                $DOPBSP->views->backend_translation->template();
            }
            
            /*
             * Initialize translation.
             */
            function init(){
                /*
                 * Initialize text if it has not been done.
                 */   
                $this->classes = apply_filters('dopbsp_filter_translation_classes', $this->classes);
                
                if (count($this->text) == 0){
                    $this->initText();
                }
                $this->text = apply_filters('dopbsp_filter_translation', $this->text);
            }
            
            /*
             * Initialize translation text.
             */
            function initText(){
                for ($i=0; $i<count($this->classes); $i++){
                    if (class_exists($this->classes[$i])){
                        new $this->classes[$i]();
                    }
                }
            }
            
            /*
             * Set PHP translation.
             * 
             * @param language (string): language code to be used, default "DOPBSP_CONFIG_TRANSLATION_DEFAULT_LANGUAGE"
             * @param use_admin_language (boolean): "true" to use the language selected in the administration area
             * 
             * @return encoded JSON
             */
            function set($language = DOPBSP_CONFIG_TRANSLATION_DEFAULT_LANGUAGE,
                         $use_admin_language = true){
                global $wpdb;
                global $DOPBSP;
                
                /*
                 * Get back end language.
                 */
                if (is_admin()
                        && $use_admin_language){
                    $language = $DOPBSP->classes->backend_language->get();
                }
                
                $translation = $wpdb->get_results('SELECT * FROM '.$DOPBSP->tables->translation.'_'.$language);
                
                if (count($this->text) != 0
                        && count($this->text) != count($translation)){
                    $DOPBSP->classes->backend_translation->database($language);
                    $translation = $wpdb->get_results('SELECT * FROM '.$DOPBSP->tables->translation.'_'.$language);
                }
                
                $DOPBSP->vars->translation_text = array();
                        
                foreach ($translation as $item){
                    $DOPBSP->vars->translation_text[$item->key_data] = str_replace('<<single-quote>>', "'", stripslashes($item->translation));
                }
            }
            
            /*
             * Set translation classes.
             */
            function setClasses(){
                /*
                 * 1. Initialize general text.
                 */
                array_push($this->classes, 'DOPBSPTranslationTextGeneral');
                
                /*
                 * 2. Initialize dashboard text.
                 */
                array_push($this->classes, 'DOPBSPTranslationTextDashboard');
                
                /*
                 * 3. Initialize calendars text.
                 */
                array_push($this->classes, 'DOPBSPTranslationTextCalendars');
                
                /*
                 * 4. Initialize events text.
                 */
                array_push($this->classes, 'DOPBSPTranslationTextEvents');
                
                /*
                 * 5. Initialize staff text.
                 */
                array_push($this->classes, 'DOPBSPTranslationTextStaff');
                
                /*
                 * 6. Initialize locations text.
                 */
                array_push($this->classes, 'DOPBSPTranslationTextLocations');
                
                /*
                 * 7. Initialize reservations text.
                 */
                array_push($this->classes, 'DOPBSPTranslationTextReservations');
                
                /*
                 * 8. Initialize locations text.
                 */
                array_push($this->classes, 'DOPBSPTranslationTextReviews');
                
                /*
                 * 9. Initialize rules text.
                 */
                array_push($this->classes, 'DOPBSPTranslationTextRules');
                
                /*
                 * 10. Initialize extras text.
                 */
                array_push($this->classes, 'DOPBSPTranslationTextExtras');
                
                /*
                 * 11. Initialize amenities text.
                 */
                array_push($this->classes, 'DOPBSPTranslationTextAmenities');
                
                /*
                 * 12. Initialize discounts text.
                 */
                array_push($this->classes, 'DOPBSPTranslationTextDiscounts');
                
                /*
                 * 13. Initialize fees text.
                 */
                array_push($this->classes, 'DOPBSPTranslationTextFees');
                
                /*
                 * 14. Initialize coupons text.
                 */
                array_push($this->classes, 'DOPBSPTranslationTextCoupons');
                
                /*
                 * 15. Initialize forms text.
                 */
                array_push($this->classes, 'DOPBSPTranslationTextForms');
                
                /*
                 * 16. Initialize templates text.
                 */
                array_push($this->classes, 'DOPBSPTranslationTextTemplates');
                
                /*
                 * 17. Initialize emails text.
                 */
                array_push($this->classes, 'DOPBSPTranslationTextEmails');
                
                /*
                 * 18. Initialize languages text.
                 */
                array_push($this->classes, 'DOPBSPTranslationTextLanguages');
                
                /*
                 * 19. Initialize translation text.
                 */
                array_push($this->classes, 'DOPBSPTranslationTextTranslation');
                
                /*
                 * 20. Initialize settings text.
                 */
                array_push($this->classes, 'DOPBSPTranslationTextSettings');
                
                /*
                 * 21. Initialize tools text.
                 */
                array_push($this->classes, 'DOPBSPTranslationTextTools');
                
                /*
                 * 22. Initialize addons text.
                 */
                array_push($this->classes, 'DOPBSPTranslationTextAddons');
                
                /*
                 * 23. Initialize themes text.
                 */
                array_push($this->classes, 'DOPBSPTranslationTextThemes');
                
                /*
                 * 24. Initialize PayPal text.
                 */
                array_push($this->classes, 'DOPBSPTranslationTextPayPal');
                
                /*
                 * 25. Initialize WooCommerce text.
                 */
                array_push($this->classes, 'DOPBSPTranslationTextWooCommerce');
                
                /*
                 * 26. Initialize custom posts text.
                 */
                array_push($this->classes, 'DOPBSPTranslationTextCustomPosts');
                
                /*
                 * 27. Initialize widgets text.
                 */
                array_push($this->classes, 'DOPBSPTranslationTextWidgets');
                
                /*
                 * 28. Initialize search text.
                 */
                array_push($this->classes, 'DOPBSPTranslationTextSearch');
                
                /*
                 * 29. Initialize cart text.
                 */
                array_push($this->classes, 'DOPBSPTranslationTextCart');
                
                /*
                 * 30. Initialize order text.
                 */
                array_push($this->classes, 'DOPBSPTranslationTextOrder');
            }
            
            /*
             * Set translation JSON.
             */
            function encodeJSON($key,
                                $text = '',
                                $pref_text = '',
                                $suff_text = ''){
                global $wpdb;
                global $DOPBSP;
                
                $json = array();
                
                $languages = $wpdb->get_results('SELECT * FROM '.$DOPBSP->tables->languages.' WHERE enabled="true"');

                foreach ($languages as $language){
                    if ($key != ''){
                        $translation = $wpdb->get_row('SELECT * FROM '.$DOPBSP->tables->translation.'_'.$language->code.' WHERE key_data="'.$key.'"');
                        array_push($json, '"'.$language->code.'": "'.$pref_text.utf8_encode($translation->text_data).$suff_text.'"' );
                    }
                    else{
                        array_push($json, '"'.$language->code.'": "'.$pref_text.utf8_encode($text).$suff_text.'"' );
                    }
                }
                
                return '{'.implode(',', $json).'}';
            }
            
            /*
             * Get text from translation JSON.
             * 
             * @param json (string): JSON string
             * @param language (string): language code
             * 
             * @return translation text
             */
            function decodeJSON($json,
                                $language = DOPBSP_CONFIG_TRANSLATION_DEFAULT_LANGUAGE){
                $translation = json_decode($json);
                $default_language = DOPBSP_CONFIG_TRANSLATION_DEFAULT_LANGUAGE;
                
                $text = isset($translation->$language) ? $translation->$language:$translation->$default_language;
                $text = utf8_decode($text);
                $text = stripslashes($text);
                $text = str_replace('<<new-line>>', "\n", $text);
                $text = str_replace('<<single-quote>>', "'", $text);
                
                return $text;
            }
            
// Check translation to see what keys are used.
            
            /*
             * Check translations keys.
             * 
             * @return HTML of unused translation keys
             */
            function check(){
                global $DOPBSP;
                
                for ($i=0; $i<count($this->text); $i++){
                    $this->text[$i]['check'] = false;
                }
                        
                $this->checkFolder($DOPBSP->paths->abs);
                
                for ($i=0; $i<count($this->text); $i++){
                    if (strpos($this->text[$i]['key'], 'PARENT_') === false){
                        if ($this->text[$i]['check'] == true){
                            // echo '<span class="dopbsp-used">'.$this->text[$i]['key'].'</span><br />';
                        }
                        else{
                            echo '<span class="dopbsp-unused">'.$this->text[$i]['key'].'</span><br />';
                        }
                    }
                }
                
                die();
            }
            
            /*
             * Check files for translation keys.
             * 
             * @param folder (string): folder to be checked
             */
            function checkFolder($folder){
                $folderData = opendir($folder);
                
                while (($file = readdir($folderData)) !== false){
                    if ($file != '.' 
                            && $file != '..'){
                        if (filetype($folder.$file) == 'file'){
                            $ext = pathinfo($folder.$file, PATHINFO_EXTENSION);
                            
                            if (($ext == 'js' 
                                            || $ext == 'php') 
                                    && strrpos($file, 'class-translation') === false){
                                $file_data = file_get_contents($folder.$file);
                                
                                for ($i=0; $i<count($this->text); $i++){
                                    if (strpos($file_data, $this->text[$i]['key']) !== false
                                            || strpos($this->text[$i]['key'], 'PAYPAL') !== false
                                            || strpos($this->text[$i]['key'], 'STRIPE') !== false){
                                        $this->text[$i]['check'] = true;
                                    }
                                }
                            }
                        }
                        elseif (filetype($folder.$file) == 'dir'){
                            $this->checkFolder($folder.$file.'/');
                        }
                    }
                }
                closedir($folderData);
            }
        }
    }
<?php

/*
* Title                   : Booking System PRO (WordPress Plugin)
* Version                 : 2.0
* File                    : views/views-backend-dashboard.php
* File Version            : 1.0.1
* Created / Last Modified : 29 July 2014
* Author                  : Dot on Paper
* Copyright               : © 2012 Dot on Paper
* Website                 : http://www.dotonpaper.net
* Description             : Booking System PRO back end dashboard views class.
*/

    if (!class_exists('DOPBSPViewsDashboard')){
        class DOPBSPViewsDashboard extends DOPBSPViews{
            /*
             * Constructor
             */
            function DOPBSPViewsDashboard(){
            }
            
            /*
             * Returns dashboard template.
             * 
             * @return calendars HTML page
             */
            function template(){
                global $DOPBSP;
                
                $this->getTranslation();
?>            
    <div class="wrap DOPBSP-admin">
        
<!-- 
    Header
-->
        <?php $this->displayHeader($DOPBSP->text('TITLE').' - '.$DOPBSP->text('DASHBOARD_TITLE')); ?>

<!--
    Content
-->
        <div class="main">
            <section class="content-wrapper">
                <h3><?php echo $DOPBSP->text('DASHBOARD_SUBTITLE'); ?></h3>
                <p><?php echo $DOPBSP->text('DASHBOARD_TEXT'); ?></p>
                
                <div id="DOPBSP-get-started" class="DOPBSP-left">
                    <h4><?php echo $DOPBSP->text('DASHBOARD_GET_STARTED'); ?></h4>
                    <ul>
                        <li>
                            <a href="<?php echo admin_url('admin.php?page=dopbsp-calendars'); ?>">
                                <span class="icon calendars"></span>
                                <?php echo $DOPBSP->text('DASHBOARD_GET_STARTED_CALENDARS'); ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo admin_url('admin.php?page=dopbsp-events'); ?>">
                                <span class="icon events"></span>
                                <?php echo $DOPBSP->text('DASHBOARD_GET_STARTED_EVENTS').' <em>('.$DOPBSP->text('SOON').')</em>'; ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo admin_url('admin.php?page=dopbsp-reservations'); ?>">
                                <span class="icon reservations"></span>
                                <?php echo $DOPBSP->text('DASHBOARD_MORE_ACTIONS_RESERVATIONS'); ?>
                            </a>
                        </li>
                    </ul>
                </div>
                
                <div id="DOPBSP-more-actions" class="DOPBSP-left">
                    <h4><?php echo $DOPBSP->text('DASHBOARD_MORE_ACTIONS'); ?></h4>
                    <ul>
                        <li>
                            <a href="<?php echo admin_url('admin.php?page=dopbsp-coupons'); ?>">
                                <span class="icon coupons"></span>
                                <?php echo $DOPBSP->text('DASHBOARD_MORE_ACTIONS_COUPONS'); ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo admin_url('admin.php?page=dopbsp-discounts'); ?>">
                                <span class="icon discounts"></span>
                                <?php echo $DOPBSP->text('DASHBOARD_MORE_ACTIONS_DISCOUNTS'); ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo admin_url('admin.php?page=dopbsp-emails'); ?>">
                                <span class="icon emails"></span>
                                <?php echo $DOPBSP->text('DASHBOARD_MORE_ACTIONS_EMAILS'); ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo admin_url('admin.php?page=dopbsp-extras'); ?>">
                                <span class="icon extras"></span>
                                <?php echo $DOPBSP->text('DASHBOARD_MORE_ACTIONS_EXTRAS'); ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo admin_url('admin.php?page=dopbsp-forms'); ?>">
                                <span class="icon forms"></span>
                                <?php echo $DOPBSP->text('DASHBOARD_MORE_ACTIONS_FORMS'); ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo admin_url('admin.php?page=dopbsp-locations'); ?>">
                                <span class="icon locations"></span>
                                <?php echo $DOPBSP->text('DASHBOARD_MORE_ACTIONS_LOCATIONS').' <em>('.$DOPBSP->text('SOON').')</em>'; ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo admin_url('admin.php?page=dopbsp-rules'); ?>">
                                <span class="icon rules"></span>
                                <?php echo $DOPBSP->text('DASHBOARD_MORE_ACTIONS_RULES'); ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo admin_url('admin.php?page=dopbsp-fees'); ?>">
                                <span class="icon fees"></span>
                                <?php echo $DOPBSP->text('DASHBOARD_MORE_ACTIONS_FEES'); ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo admin_url('admin.php?page=dopbsp-templates'); ?>">
                                <span class="icon templates"></span>
                                <?php echo $DOPBSP->text('DASHBOARD_MORE_ACTIONS_TEMPLATES').' <em>('.$DOPBSP->text('SOON').')</em>'; ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo admin_url('admin.php?page=dopbsp-settings'); ?>">
                                <span class="icon settings"></span>
                                <?php echo $DOPBSP->text('DASHBOARD_MORE_ACTIONS_SETTINGS'); ?>
                            </a>
                        </li>
                        <li>
                            <a href="<?php echo admin_url('admin.php?page=dopbsp-translation'); ?>">
                                <span class="icon translation"></span>
                                <?php echo $DOPBSP->text('DASHBOARD_MORE_ACTIONS_TRANSLATION'); ?>
                            </a>
                        </li>
                    </ul>
                </div>
            </section>
            <script type="text/javascript">
//                window.location.href = '<?php echo admin_url('admin.php?page=dopbsp-calendars'); ?>';
            </script>
        </div>
    </div>       
<?php
            }
        }
    }
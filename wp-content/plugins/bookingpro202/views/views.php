<?php

/*
* Title                   : Booking System PRO (WordPress Plugin)
* Version                 : 2.0
* File                    : views/views.php
* File Version            : 1.0.1
* Created / Last Modified : 02 December 2014
* Author                  : Dot on Paper
* Copyright               : © 2012 Dot on Paper
* Website                 : http://www.dotonpaper.net
* Description             : Booking System PRO views class.
*/

    if (!class_exists('DOPBSPViews')){
        class DOPBSPViews{
            /*
             * Private variables.
             */
            private $classes = array();
            
            /*
             * Constructor
             */
            function DOPBSPViews(){
                $this->setClasses();
                
                add_action('init', array(&$this, 'init'));
            }
            
            /*
             * Initialize view.
             */
            function init(){
                $this->classes = apply_filters('dopbsp_filter_views_classes', $this->classes);
                
                $this->initClasses();
            }
            
            /*
             * Initialize view classes.
             */
            function initClasses(){
                $classes = $this->classes;
                
                for ($i=0; $i<count($classes); $i++){
                    if (class_exists($classes[$i]['name'])){
                        $this->$classes[$i]['key'] = new $classes[$i]['name']();
                    }
                }
            }
            
            /*
             * Set view classes.
             */
            function setClasses(){
                /*
                 * Initialize back end view.
                 */
                array_push($this->classes, array('key' => 'backend',
                                                 'name' => 'DOPBSPViewsBackEnd'));
                
                /*
                 * Initialize front end view.
                 */
                array_push($this->classes, array('key' => 'frontend',
                                                 'name' => 'DOPBSPViewsFrontEnd'));
                
                /*
                 * Initialize addons view classes.
                 */
                array_push($this->classes, array('key' => 'backend_addons',
                                                 'name' => 'DOPBSPViewsBackEndAddons'));
                
                /*
                 * Initialize amenities view classes.
                 */
                array_push($this->classes, array('key' => 'backend_amenities',
                                                 'name' => 'DOPBSPViewsBackEndAmenities'));
                
                /*
                 * Initialize calendars view classes.
                 */
                array_push($this->classes, array('key' => 'backend_calendars',
                                                 'name' => 'DOPBSPViewsBackEndCalendars'));
                
                /*
                 * Initialize coupons view classes.
                 */
                array_push($this->classes, array('key' => 'backend_coupons',
                                                 'name' => 'DOPBSPViewsBackEndCoupons'));
                array_push($this->classes, array('key' => 'backend_coupon',
                                                 'name' => 'DOPBSPViewsBackEndCoupon'));
                
                /*
                 * Initialize dashboard view classes.
                 */
                array_push($this->classes, array('key' => 'backend_dashboard',
                                                 'name' => 'DOPBSPViewsBackEndDashboard'));
                array_push($this->classes, array('key' => 'backend_dashboard_server',
                                                 'name' => 'DOPBSPViewsBackEndDashboardServer'));
                array_push($this->classes, array('key' => 'backend_dashboard_start',
                                                 'name' => 'DOPBSPViewsBackEndDashboardStart'));
                
                /*
                 * Initialize discounts view classes.
                 */
                array_push($this->classes, array('key' => 'backend_discounts',
                                                 'name' => 'DOPBSPViewsBackEndDiscounts'));
                array_push($this->classes, array('key' => 'backend_discount',
                                                 'name' => 'DOPBSPViewsBackEndDiscount'));
                array_push($this->classes, array('key' => 'backend_discount_items',
                                                 'name' => 'DOPBSPViewsBackEndDiscountItems'));
                array_push($this->classes, array('key' => 'backend_discount_item',
                                                 'name' => 'DOPBSPViewsBackEndDiscountItem'));
                array_push($this->classes, array('key' => 'backend_discount_item_rule',
                                                 'name' => 'DOPBSPViewsBackEndDiscountItemRule'));
                
                /*
                 * Initialize emails view classes.
                 */
                array_push($this->classes, array('key' => 'backend_emails',
                                                 'name' => 'DOPBSPViewsBackEndEmails'));
                array_push($this->classes, array('key' => 'backend_email',
                                                 'name' => 'DOPBSPViewsBackEndEmail'));
                
                /*
                 * Initialize events view classes.
                 */
                array_push($this->classes, array('key' => 'backend_events',
                                                 'name' => 'DOPBSPViewsBackEndEvents'));
                
                /*
                 * Initialize extras view classes.
                 */
                array_push($this->classes, array('key' => 'backend_extras',
                                                 'name' => 'DOPBSPViewsBackEndExtras'));
                array_push($this->classes, array('key' => 'backend_extra',
                                                 'name' => 'DOPBSPViewsBackEndExtra'));
                array_push($this->classes, array('key' => 'backend_extra_groups',
                                                 'name' => 'DOPBSPViewsBackEndExtraGroups'));
                array_push($this->classes, array('key' => 'backend_extra_group',
                                                 'name' => 'DOPBSPViewsBackEndExtraGroup'));
                array_push($this->classes, array('key' => 'backend_extra_group_item',
                                                 'name' => 'DOPBSPViewsBackEndExtraGroupItem'));
                
                /*
                 * Initialize fees view classes.
                 */
                array_push($this->classes, array('key' => 'backend_fees',
                                                 'name' => 'DOPBSPViewsBackEndFees'));
                array_push($this->classes, array('key' => 'backend_fee',
                                                 'name' => 'DOPBSPViewsBackEndFee'));
                
                /*
                 * Initialize forms view classes.
                 */
                array_push($this->classes, array('key' => 'backend_forms',
                                                 'name' => 'DOPBSPViewsBackEndForms'));
                array_push($this->classes, array('key' => 'backend_form',
                                                 'name' => 'DOPBSPViewsBackEndForm'));
                array_push($this->classes, array('key' => 'backend_form_fields',
                                                 'name' => 'DOPBSPViewsBackEndFormFields'));
                array_push($this->classes, array('key' => 'backend_form_field',
                                                 'name' => 'DOPBSPViewsBackEndFormField'));
                array_push($this->classes, array('key' => 'backend_form_field_select_option',
                                                 'name' => 'DOPBSPViewsBackEndFormFieldSelectOption'));
                
                /*
                 * Initialize languages view classes.
                 */
                array_push($this->classes, array('key' => 'backend_languages',
                                                 'name' => 'DOPBSPViewsBackEndLanguages'));
                
                /*
                 * Initialize locations view classes.
                 */
                array_push($this->classes, array('key' => 'backend_locations',
                                                 'name' => 'DOPBSPViewsBackEndLocations'));
                array_push($this->classes, array('key' => 'backend_location',
                                                 'name' => 'DOPBSPViewsBackEndLocation'));
                
                /*
                 * Initialize reservations view classes.
                 */
                array_push($this->classes, array('key' => 'backend_reservations',
                                                 'name' => 'DOPBSPViewsBackEndReservations'));
                array_push($this->classes, array('key' => 'backend_reservations_list',
                                                 'name' => 'DOPBSPViewsBackEndReservationsList'));
                array_push($this->classes, array('key' => 'backend_reservation',
                                                 'name' => 'DOPBSPViewsBackEndReservation'));
                array_push($this->classes, array('key' => 'backend_reservation_coupon',
                                                 'name' => 'DOPBSPViewsBackEndReservationCoupon'));
                array_push($this->classes, array('key' => 'backend_reservation_details',
                                                 'name' => 'DOPBSPViewsBackEndReservationDetails'));
                array_push($this->classes, array('key' => 'backend_reservation_discount',
                                                 'name' => 'DOPBSPViewsBackEndReservationDiscount'));
                array_push($this->classes, array('key' => 'backend_reservation_extras',
                                                 'name' => 'DOPBSPViewsBackEndReservationExtras'));
                array_push($this->classes, array('key' => 'backend_reservation_fees',
                                                 'name' => 'DOPBSPViewsBackEndReservationFees'));
                array_push($this->classes, array('key' => 'backend_reservation_form',
                                                 'name' => 'DOPBSPViewsBackEndReservationForm'));
                
                /*
                 * Initialize reviews view classes.
                 */
                array_push($this->classes, array('key' => 'backend_reviews',
                                                 'name' => 'DOPBSPViewsBackEndReviews'));
                
                /*
                 * Initialize rules view classes.
                 */
                array_push($this->classes, array('key' => 'backend_rules',
                                                 'name' => 'DOPBSPViewsBackEndRules'));
                array_push($this->classes, array('key' => 'backend_rule',
                                                 'name' => 'DOPBSPViewsBackEndRule'));
                
                /*
                 * Initialize search view classes.
                 */
                array_push($this->classes, array('key' => 'backend_searches',
                                                 'name' => 'DOPBSPViewsBackEndSearches'));
                array_push($this->classes, array('key' => 'backend_search',
                                                 'name' => 'DOPBSPViewsBackEndSearch'));
                array_push($this->classes, array('key' => 'frontend_search',
                                                 'name' => 'DOPBSPViewsFrontEndSearch'));
                array_push($this->classes, array('key' => 'frontend_search_results',
                                                 'name' => 'DOPBSPViewsFrontEndSearchResults'));
                array_push($this->classes, array('key' => 'frontend_search_results_list',
                                                 'name' => 'DOPBSPViewsFrontEndSearchResultsList'));
                array_push($this->classes, array('key' => 'frontend_search_results_grid',
                                                 'name' => 'DOPBSPViewsFrontEndSearchResultsGrid'));
                array_push($this->classes, array('key' => 'frontend_search_sidebar',
                                                 'name' => 'DOPBSPViewsFrontEndSearchSidebar'));
                array_push($this->classes, array('key' => 'frontend_search_sort',
                                                 'name' => 'DOPBSPViewsFrontEndSearchSort'));
                array_push($this->classes, array('key' => 'frontend_search_view',
                                                 'name' => 'DOPBSPViewsFrontEndSearchView'));
                
                /*
                 * Initialize settings view classes.
                 */
                array_push($this->classes, array('key' => 'backend_settings',
                                                 'name' => 'DOPBSPViewsBackEndSettings'));
                array_push($this->classes, array('key' => 'backend_settings_calendar',
                                                 'name' => 'DOPBSPViewsBackEndSettingsCalendar'));
                array_push($this->classes, array('key' => 'backend_settings_notifications',
                                                 'name' => 'DOPBSPViewsBackEndSettingsNotifications'));
                array_push($this->classes, array('key' => 'backend_settings_payment_gateways',
                                                 'name' => 'DOPBSPViewsBackEndSettingsPaymentGateways'));
                array_push($this->classes, array('key' => 'backend_settings_paypal',
                                                 'name' => 'DOPBSPViewsBackEndSettingsPayPal'));
                array_push($this->classes, array('key' => 'backend_settings_search',
                                                 'name' => 'DOPBSPViewsBackEndSettingsSearch'));
                array_push($this->classes, array('key' => 'backend_settings_users',
                                                 'name' => 'DOPBSPViewsBackEndSettingsUsers'));
                
                /*
                 * Initialize staff view classes.
                 */
                array_push($this->classes, array('key' => 'backend_staff',
                                                 'name' => 'DOPBSPViewsBackEndStaff'));
                
                /*
                 * Initialize templates view classes.
                 */
                array_push($this->classes, array('key' => 'backend_templates',
                                                 'name' => 'DOPBSPViewsBackEndTemplates'));
                
                /*
                 * Initialize themes view classes.
                 */
                array_push($this->classes, array('key' => 'backend_themes',
                                                 'name' => 'DOPBSPViewsBackEndThemes'));
                
                /*
                 * Initialize tools view classes.
                 */
                array_push($this->classes, array('key' => 'backend_tools',
                                                 'name' => 'DOPBSPViewsBackEndTools'));
                array_push($this->classes, array('key' => 'backend_tools_repair_calendars_settings',
                                                 'name' => 'DOPBSPViewsBackEndToolsRepairCalendarsSettings'));
                
                /*
                 * Initialize translation view classes.
                 */
                array_push($this->classes, array('key' => 'backend_translation',
                                                 'name' => 'DOPBSPViewsBackEndTranslation'));
            }
        }
    }
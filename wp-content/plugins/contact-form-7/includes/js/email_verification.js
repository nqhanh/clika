// First we trigger the form submit event
$('.wpcf7-submit').click(function () {
        // We remove the error to avoid duplicate errors
	$('.wpcf7-not-valid-tip').remove();

        // We create a variable to store our error message
	var errorMsg = $('<span class="wpcf7-not-valid-tip">メールアドレスを正しく入力してください。</span>');

        // Then we check our values to see if they match
        // If they do not match we display the error and we do not allow form to submit
	if ($('.email').find('input').val() !== $('.email-confirm').find('input').val()) {
		$('.wpcf7-not-valid-tip').remove();
		errorMsg.insertAfter($('.email-confirm').find('input'));

		return false;
	} else {
                // If they do match we remove the error and we submit the form
		$('.wpcf7-not-valid-tip').remove();

		return true;
	}
});
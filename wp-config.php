<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'alv_booking');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'A{I-q::y|xYv]:8Fjp/N6()_nrd<A8r:FI[.h.WS^361fW;pi~6Oka4ZpF9Cd|=n');
define('SECURE_AUTH_KEY',  ';JyH%L&9qwMknltaw|qkf|HV_/o#V[wWQiD;x%tT_O@Wm#`-]T^LJ,(WbsTeiJ#z');
define('LOGGED_IN_KEY',    ']KQ3]K(( <{#[)j=0=#;[k}_0x-zw#)*kDxhgo0A>5KR^(r|M9:o#*qcYL,:%DZw');
define('NONCE_KEY',        'J_>_>=h}vMZUyj6.0@9.+|N`h{1Sc )sL[95uI}IzbDX21a,hgsMfcv}[dW+UQ[1');
define('AUTH_SALT',        'vU|;X#wdWllOEx+Pv0Scs|rU L%{nO?z,TG7{Et[IWH,%~xG&aQ`EGKjpXAf8B0b');
define('SECURE_AUTH_SALT', 'S`q52 9Zs<UqQaLY/6f[WpaS-a}fEuO.:k<hl^d1di-nc s9P|Aq[e?&AR.l_F#{');
define('LOGGED_IN_SALT',   '|D%7+Yz1y$]J/-0s~pehZ{rLHH{?T/Nf00%!S?B`!_BaI*PK_oh$rV R5%8%!L.b');
define('NONCE_SALT',       '(d50Md)Oiv#.q@*ur:Y8Y~lU?qczjw_ {!K<gHm^*{9+$7*I_Cbb<VqwmiEoc^u^');
define('WP_MEMORY_LIMIT', '128M');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';
set_time_limit(60);

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
